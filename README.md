# minecraft-server

A containerised [Minecraft](https://minecraft.net) server using [lazymc](https://github.com/timvisee/lazymc) to launch on demand.

## Requirements

To launch the container, [Docker](https://www.docker.com/products/docker-desktop) is required.

## Getting Started

Create a Docker volume to store the server data:

```
$ docker volume create minecraft-server_minecraft-server
```

*WARNING: Do not use a directory mount in Windows to store the data if using WSL2 for the Docker backend. Crossing the filesystem boundary may cause IO errors when reading and writing chunks.*

## Usage

Launch the container using the provided `docker-compose.yml` file with the following command:

```
$ docker compose up -d
```

The following Docker command is equivalent:

```
$ docker run -it -d --restart on-failure \
    -v minecraft-server_minecraft-server:/var/minecraft \
    -p 25565:25565 \
    --name=minecraft-server \
    registry.gitlab.com/cwassall/minecraft-server:latest
```

## Configuration

Environment variables can be passed to the container to set or override settings. After running the server for the first time, configuration can also be done by editing the files in the mounted volume.

* `SERVER_IDLE`: `lazymc`

    * `lazymc`: Use [lazymc](https://github.com/timvisee/lazymc) to launch on demand.
    * `vanilla`: Run the server directly.

* `SERVER_TYPE`: `vanilla`

    * `fabric`: Use the [Fabric server](https://fabricmc.net).
    * `forge`: Use the [Forge server](https://files.minecraftforge.net).
    * `paperclip`: Use [Paperclip](https://github.com/PaperMC/Paperclip) to launch the [Paper server](https://papermc.io/).
    * `vanilla`: Use the vanilla [Minecraft server](https://minecraft.net).

* `MINECRAFT_TYPE`: `release`

    * `release`: Use a release version of the server.
    * `snapshot`: Use a snapshot version of the server.

* `MINECRAFT_VERSION`: `latest`

    * `latest`: Use the latest version of the server.
    * `1.19.2` or any valid version: Use the specified version of the server.

* `MOTD`: `A Minecraft Server`

    * `A Minecraft Server` or any valid string: The Message of the Day.

* `GAMEMODE`: `survival`

    * `adventure`: Run in adventure mode.
    * `creative`: Run in creative mode.
    * `spectator`: Run in spectator mode.
    * `survival`: Run in survival mode.

* `DIFFICULTY`: `easy`

    * `easy`: Run in easy mode.
    * `normal`: Run in normal mode.
    * `hard`: Run in hard mode.
    * `hardcore`: Run in hardcore mode.

* `COMMAND_BLOCKS`: `false`

    * `true`: Allow Command Blocks to be spawned.
    * `false`: Prevent Command Blocks being spawned.

* `VIEW_DISTANCE`: `10`

    * `10` or any valid number: The maximum view distance.

* `SIMULATION_DISTANCE`: `10`

    * `10` or any valid number: The maximum simulation distance.

* `PLAYER_IDLE_TIMEOUT`: `0`

    * `0` or any valid number: The number of minutes to wait before kicking and idle player, or `0` to never kick idle players.

* `JAVA_MEMORY`: `2g`

    * `2g` or any valid memory string: The amount of memory to allocate to Java.

## Management

To connect to the server's terminal run the command:

```
$ docker attach minecraft-server
```

To disconnect use the key sequence `CTRL-p CTRL-q`.

## Backups

A one-off backup can be created with the commands:

Compressed archive:

```
$ mkdir -p ./backup
$ docker stop minecraft-server
$ docker run --rm -it -v ${PWD}:/backup -v minecraft-server_minecraft-server:/var/minecraft alpine sh -c 'tar -czvf /backup/backup.tar.gz /var/minecraft'
$ docker start minecraft-server
```

Borg backup:

```
$ mkdir -p ./backup
$ docker stop minecraft-server
$ docker run --rm -v ${PWD}/backup:/backup -v minecraft-server_minecraft-server:/data -e BORG_REPO=/backup -e BACKUP_DIRS=/data -e ARCHIVE=minecraft-server-$(date +%Y-%m-%d) -e COMPRESSION=lz4 -e PRUNE=1 -e BORG_PASSPHRASE=BORG_PASSPHRASE pschiffe/borg
$ docker start minecraft-server
```

A backup can be restored with the commands:

Compressed archive:

```
$ docker run --rm -it -v ${PWD}:/backup -v minecraft-server_minecraft-server:/var/minecraft alpine sh -c 'tar -xzvf /backup/backup.tar.gz -C /var/minecraft'
$ docker compose up -d
```

Borg backup:

```
$ docker run --rm -v ${PWD}/backup:/backup -v minecraft-server_minecraft-server:/data -e BORG_REPO=/backup -e ARCHIVE=minecraft-server-$(date +%Y-%m-%d) -e EXTRACT_TO=/ -e BORG_PASSPHRASE=BORG_PASSPHRASE pschiffe/borg
$ docker compose up -d
```

## Multiple Servers

The following Docker commands will run two servers on different ports, provided the volumes exist:

```
$ docker run -it -d --restart on-failure \
    -v minecraft-server-1:/var/minecraft \
    -p 25565:25565 \
    --name=minecraft-server-1 \
    registry.gitlab.com/cwassall/minecraft-server:latest

$ docker run -it -d --restart on-failure \
    -v minecraft-server-2:/var/minecraft \
    -p 25566:25565 \
    --name=minecraft-server-2 \
    registry.gitlab.com/cwassall/minecraft-server:latest
```
