FROM openjdk:17-slim AS base

RUN apt update && \
    apt install -y --no-install-recommends nano curl wget jq || true && \
    dpkg --purge --force-all libc-bin && \
    apt install -y --no-install-recommends nano curl wget jq && \
    rm -rf /var/lib/apt/lists/*

FROM base AS install

COPY launch.sh launch
RUN chmod +x launch

FROM install AS server
VOLUME /var/minecraft
ENTRYPOINT ["./launch"]
