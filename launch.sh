#!/bin/bash

INSTALLDIR=/var/minecraft

if [ ! "$SERVER_IDLE" ]; then SERVER_IDLE=lazymc; fi
if [ ! "$SERVER_TYPE" ]; then SERVER_TYPE=vanilla; fi
if [ ! "$MINECRAFT_TYPE" ]; then MINECRAFT_TYPE=release; fi
if [ ! "$MINECRAFT_VERSION" ]; then MINECRAFT_VERSION=latest; fi
if [ ! "$JAVA_MEMORY" ]; then JAVA_MEMORY=2G; fi
if [ ! "$JAVA_OPTS" ]; then JAVA_OPTS="-XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true"; fi

# Handle incompatible server types on existing installs by creating an install for each
mkdir -p $INSTALLDIR/$SERVER_TYPE
cd $INSTALLDIR/$SERVER_TYPE

if [ "$SERVER_IDLE" = "lazymc" ]; then
    # Download or update lazymc
    LAZYMC_VERSION=$(wget -qO - https://api.github.com/repos/timvisee/lazymc/releases/latest | jq -r .tag_name)
    LAZYMC_URL="https://github.com/timvisee/lazymc/releases/download/$LAZYMC_VERSION/lazymc-$LAZYMC_VERSION-linux-x64-static"
    if [ -e lazymc ]
    then
        rm -f lazymc
    fi
    wget -O lazymc $LAZYMC_URL
    chmod a+x lazymc

    # Create the lazymc config if it doesn't exist
    if [ ! -e lazymc.toml ]
    then
        ./lazymc config generate
    fi
fi

# Download or update the specified Minecraft server type
VERSIONS_JSON=$(curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json)
if [ "$MINECRAFT_VERSION" = "latest" ]; then VERSION=$(echo $VERSIONS_JSON | jq -r ".latest | .$MINECRAFT_TYPE"); else VERSION=$MINECRAFT_VERSION; fi
if [ "$SERVER_TYPE" = "fabric" ]
then
    SERVER_JAR=fabric-server-launch.jar
    if [ ! -e $SERVER_JAR ]
    then
        curl -L -o fabric-installer.jar https://maven.fabricmc.net/net/fabricmc/fabric-installer/0.11.1/fabric-installer-0.11.1.jar
        java -jar fabric-installer.jar server -downloadMinecraft -mcversion $VERSION
        rm fabric-installer.jar
    fi
elif [ "$SERVER_TYPE" = "paperclip" ]
then
    BUILD_JSON=$(curl -H "Accept-Encoding: identity" -H "Accept-Language: en" -L -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4.212 Safari/537.36" https://papermc.io/api/v2/projects/paper/versions/$VERSION)
    BUILD=$(echo "$BUILD_JSON" | jq .builds[-1])
    SERVER_JAR=paperclip-${VERSION}-${BUILD}.jar
    if [ ! -e $SERVER_JAR ]
    then
        curl -H "Accept-Encoding: identity" -H "Accept-Language: en" -L -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4.212 Safari/537.36" -o $SERVER_JAR "https://papermc.io/api/v2/projects/paper/versions/$VERSION/builds/$BUILD/downloads/paper-$VERSION-$BUILD.jar"
    fi
else
    SERVER_JAR=server-${VERSION}.jar
    if [ ! -e $SERVER_JAR ]
    then
        VERSION_URL=$(echo $VERSIONS_JSON | jq -r ". .versions[] | select(.id == \"$VERSION\") | .url")
        LATEST_JSON=$(curl -s $VERSION_URL)
        SERVER_URL=$(echo $LATEST_JSON | jq -r ".downloads .server .url")
        wget $SERVER_URL -O $SERVER_JAR
    fi
    if [ "$SERVER_TYPE" = "forge" ]
    then
        JAVA_OPTS="$JAVA_OPTS @libraries/net/minecraftforge/forge/1.19.2-43.2.13/unix_args.txt"
        if [ ! -e forge-installer.jar ]
        then
            INSTALLER=$(wget -qO- https://files.minecraftforge.net/net/minecraftforge/forge/index_$VERSION.html | grep -o '<a[^>]* href="[^"]*"[^>]*>(Direct Download)</a>' | sed 's/<a[^>]* href="//' | sed 's/".*//' | grep 'installer.jar' -m 1)
            curl -L -o forge-installer.jar $INSTALLER
            java -jar forge-installer.jar --installServer
        fi
    fi
fi

# Accept the EULA
echo "eula=true" > eula.txt

# Create the default server properties
if [ ! -e server.properties ]; then
    cat <<EOF > server.properties
enable-jmx-monitoring=false
rcon.port=25575
level-seed=
gamemode=survival
enable-command-block=false
enable-query=false
generator-settings={}
level-name=world
motd=A Minecraft World
query.port=25565
pvp=true
generate-structures=true
difficulty=easy
network-compression-threshold=256
require-resource-pack=false
max-tick-time=60000
use-native-transport=true
max-players=20
online-mode=true
enable-status=true
allow-flight=false
broadcast-rcon-to-ops=true
view-distance=10
server-ip=
resource-pack-prompt=
allow-nether=true
server-port=25565
enable-rcon=false
sync-chunk-writes=true
op-permission-level=4
prevent-proxy-connections=false
hide-online-players=false
resource-pack=
entity-broadcast-range-percentage=100
simulation-distance=10
rcon.password=
player-idle-timeout=0
force-gamemode=false
rate-limit=0
hardcore=false
white-list=true
broadcast-console-to-ops=true
spawn-npcs=true
spawn-animals=true
function-permission-level=2
level-type=default
text-filtering-config=
spawn-monsters=true
enforce-whitelist=true
resource-pack-sha1=
spawn-protection=16
max-world-size=29999984
EOF
fi

# Override server properties
if [ "$MOTD" ]; then sed -i -e "s/motd=.*/motd=$MOTD/" server.properties; fi
if [ "$GAMEMODE" ]; then sed -i -e "s/gamemode=.*/gamemode=$GAMEMODE/" server.properties; fi
if [ "$DIFFICULTY" ]; then sed -i -e "s/difficulty=.*/difficulty=$DIFFICULTY/" server.properties; fi
if [ "$COMMAND_BLOCKS" ]; then sed -i -e "s/enable-command-block=.*/enable-command-block=$COMMAND_BLOCKS/" server.properties; fi
if [ "$VIEW_DISTANCE" ]; then sed -i -e "s/view-distance=.*/view-distance=$VIEW_DISTANCE/" server.properties; fi
if [ "$SIMULATION_DISTANCE" ]; then sed -i -e "s/simulation-distance=.*/simulation-distance=$SIMULATION_DISTANCE/" server.properties; fi
if [ "$PLAYER_IDLE_TIMEOUT" ]; then sed -i -e "s/player-idle-timeout=.*/player-idle-timeout=$PLAYER_IDLE_TIMEOUT/" server.properties; fi

# Determine launch command
COMMAND="java -server -Xms${JAVA_MEMORY} -Xmx${JAVA_MEMORY} ${JAVA_OPTS} -jar ${SERVER_JAR} nogui"
if [ "$SERVER_IDLE" = "lazymc" ]
then
    if [ "$SERVER_TYPE" = "forge" ]
    then
        sed -i -e "s/#forge = false/forge = true/" lazymc.toml
    fi

    sed -i -e "s/#version =.*/version = \"$VERSION\"/" lazymc.toml
    sed -i -e "s|command =.*|command = \"$COMMAND\"|" lazymc.toml

    ./lazymc start
else
    $COMMAND
fi
